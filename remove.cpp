#include <string.h>
#include <dirent.h>
#include <iostream>
#include <unistd.h>

int removedirectoryrecursively(const char *dirname)
{
  DIR *dir;
  struct dirent *entry;
  char path[PATH_MAX];

  dir = opendir(dirname);
  if (dir == NULL) {
    printf("Error open directiry \"%s\"", dirname);
    return 1;
  }

  while ((entry = readdir(dir)) != NULL) {
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
      snprintf(path, (size_t) PATH_MAX, "%s/%s", dirname, entry->d_name);
      if (entry->d_type == DT_DIR) {
        removedirectoryrecursively(path);
      } else {
        remove(path);
      }
    }
  }
  closedir(dir);
  rmdir(dirname);

  return 0;
}

int main() {
  char path[] = "log";
  removedirectoryrecursively(path);
  return 0;
}
