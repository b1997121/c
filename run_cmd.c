#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void char_append(char *str1, char *str2, char *tmp_str, int str1_size, int str2_size)
{
  for (int i = 0; i < str1_size; i++)
    tmp_str[i] = str1[i];

  for (int i = 0; i < str2_size; i++)
    tmp_str[i+str1_size] = str2[i];
}

void run_command()
{

}

int main (int argc, char **argv)
{
  if (argc < 2) return 1;
  char *command = argv[1];
  FILE *fp;
  char buf[1024];
  char *rsp="", *append_str, *tmp_str;
  int rsp_size = 0, buf_size, return_code;

  fp = popen(command, "r");
  while (fgets(buf, 1024, fp) != NULL) {
    buf_size = strlen(buf);
    tmp_str = malloc((rsp_size + buf_size) * sizeof(char));
    char_append(rsp, buf, tmp_str, rsp_size, buf_size);
    rsp = tmp_str;
    rsp_size = strlen(rsp);
  }
  free(tmp_str);

  return_code = pclose(fp);
  printf("The exit status is: %d\n", return_code);

  return 0;
}

