#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstring>
#include <dirent.h>
#include <string>
#include <fstream>
#include <ctime>
#include <getopt.h>

using namespace std;

const string VERSION = "v1.1.3";
const string BUILD = "20200620";

// Save log's directory
const string LOG_DIR = "log";
const string SMART_LOG_DIR = "smart_log";
// Save test procedure log file
const string RUNNING_LOG = "test.log";

// Total loop number
static int gsLoopNum = -1;
// Run test type
static int gsRunType = -1;
// Fail stop
static bool gsFailStop = false;

string GetTimestamp() {
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
  string timestamp(buffer);

  return timestamp;
}

void OutputMsg(string msg, bool need_timestamp=false) {
  ofstream log_file;
  log_file.open(RUNNING_LOG, ios::app);
  if (need_timestamp) msg = "\n[" + GetTimestamp() + "] " + msg;
  log_file << msg << endl;
  log_file.close();
  cout << msg << endl;
}

void ExecCmd(string command, string* result, int* exit_code, bool show_cmd=false) {
  if (show_cmd) OutputMsg(command);

  char *c_cmd = &command[0];
  char buf[1024];
  FILE *fp;

  fp = popen(c_cmd, "r");
  while (fgets(buf, 1024, fp) != nullptr)
    *result += buf;

  *exit_code = pclose(fp);

  sleep(1);
}

void RemoveDirectoryRecursively(const char *dirname) {
  DIR *dir;
  struct dirent *entry;
  char path[PATH_MAX];

  dir = opendir(dirname);
  if (dir == nullptr) printf("Error open directiry \"%s\"", dirname);

  while ((entry = readdir(dir)) != nullptr) {
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
      snprintf(path, (size_t) PATH_MAX, "%s/%s", dirname, entry->d_name);
      if (entry->d_type == DT_DIR)
        RemoveDirectoryRecursively(path);
      else
        remove(path);
    }
  }
  closedir(dir);
  rmdir(dirname);
}

void CreateDir(string dir_name, bool exist_remove=false) {
  struct stat st;
  char *c_dir = &dir_name[0];

  if (stat(c_dir, &st) == 0 && exist_remove) RemoveDirectoryRecursively(c_dir);

  if (mkdir(c_dir, 0777) != 0) {
    cout << "Create " << dir_name << " FAILED!!" << endl;
    exit(1);
  }
}

void SaveLog(string cmd, string log_name, string log_path, string dev) {
  string log_cmd, result;
  int exit_code;

  if (cmd.find("smartctl") != string::npos) {
    size_t pos = 0;
    int sdx_index = cmd.find("sdx");
    string disk;
    while ((pos = dev.find("\n")) != string::npos) {
      disk = dev.substr(0, pos);
      dev.erase(0, pos + 1);
      log_cmd = cmd.replace(sdx_index, disk.length(), disk) + " > " + log_path + disk + "_" + log_name;
      ExecCmd(log_cmd, &result, &exit_code, false);
    }
  } else {
    log_cmd = cmd + " > " + log_path + log_name;
    ExecCmd(log_cmd, &result, &exit_code, false);
  }
}

string StrReplace(string main_str, string t_str, string r_str) {
  size_t pos = 0;
  string return_str = "";
  while ((pos = main_str.find(t_str)) != string::npos)
    main_str.replace(pos, t_str.length(), r_str);

  return main_str;
}

void CheckSMARTAttributes(string log_path, string log_name, string dev) {
  int pos;
  string disk, smart_log_name, item, value;
  while ((pos = dev.find("\n")) != string::npos) {
    smart_log_name = log_path + dev.substr(0, pos) + "_" + log_name;
    dev.erase(0, pos + 1);
    ifstream logfile (smart_log_name);
    string line;
    while (getline(logfile, line)) {
      item = line.substr(0, line.find(":"));
      value = line.substr(line.find(":")+1, line.length());
      if (value != "0") {
        OutputMsg(item + " value is not 0");
        exit(1);
      }
    }
  }
}

void DiffLog(string log_path, string log_name, string dev, string golden_path) {
  int exit_code;
  string result, loop_log, golden_log;
  string final_result = "PASS";
  OutputMsg("Check " + log_name, true);
  if (log_name.find("smart") != string::npos) {
    int pos;
    while ((pos = dev.find("\n")) != string::npos) {
      loop_log = log_path + dev.substr(0, pos) + "_" + log_name;
      golden_log = golden_path + dev.substr(0, pos) + "_" + log_name;
      dev.erase(0, pos + 1);
      ExecCmd("diff -q " + loop_log + " " + golden_log, &result, &exit_code);
      if (exit_code) {
        OutputMsg("Golden log: " + golden_log);
        OutputMsg("loop log: " + loop_log);
        final_result = "FAIL";
      }
    }
  } else if (log_name.find("speed") != string::npos) {
    int pos;
    while ((pos = dev.find("\n")) != string::npos) {
      result = "";
      loop_log = log_path + dev.substr(0, pos) + "_" + log_name;
      dev.erase(0, pos + 1);
      ExecCmd("awk '{if($6 == $9) {print \"same\"} else { print \"diff\"}}' " + loop_log, &result, &exit_code);
      if (result == "") {
        OutputMsg("loop log: " + loop_log);
        final_result = "FAIL";
      }
    }
  } else {
    loop_log = log_path + log_name;
    golden_log = golden_path + log_name;
    ExecCmd("exec bash -c 'diff -q <(egrep -c \"^sd|sd. $\" " + loop_log + ") <(egrep -c \"^sd|sd. $\" " + golden_log + ")'", &result, &exit_code);
    if (exit_code) {
      OutputMsg("Golden log: " + golden_log);
      OutputMsg("loop log: " + loop_log);
      final_result = "FAIL";
    }
  }
  OutputMsg("Result: " + final_result);
  if (final_result == "FAIL" && gsFailStop) exit(1);
}

void LogProcess(string dir_name, string dev) {
  string log_path = LOG_DIR + "/" + dir_name + "/";
  string smart_log_path = SMART_LOG_DIR + "/" + dir_name + "/";
  string cmds[] = {
    "smartctl --all /dev/sdx | grep -i gb/s",
    "lsscsi",
    "lsblk",
    "smartctl -A /dev/sdx | awk '/Reallocated|Current/ {print $2\":\"$NF}'"};
  string log_name[] = {"HDD_speed.log", "lsscsi.log", "lsblk.log", "smart_data.log"};

  OutputMsg("Save " + StrReplace(dir_name, "_", " ") + " log", true);
  CreateDir(log_path);
  CreateDir(smart_log_path);

  for (int i=0; i<(sizeof(cmds)/sizeof(*cmds)); i++) {
    if (i != 3)
      SaveLog(cmds[i], log_name[i], log_path, dev);
    else
      SaveLog(cmds[i], log_name[i], smart_log_path, dev);
  }

  if (dir_name.find("golden") != string::npos) {
    CheckSMARTAttributes(smart_log_path, log_name[3], dev);
  } else {
    for (int i=0; i<(sizeof(cmds)/sizeof(*cmds)); i++) {
      string golden_path = "";
      if (i != 3)
        golden_path = LOG_DIR + "/";
      else
        golden_path = SMART_LOG_DIR + "/";
      if (gsRunType == 1) {
        golden_path += dir_name.replace(0, dir_name.find("_"), "golden") + "/";
      } else {
        golden_path += "golden/";
      }
      if (i != 3)
        DiffLog(log_path, log_name[i], dev, golden_path);
      else
        DiffLog(smart_log_path, log_name[i], dev, golden_path);
    }
  }
}

void SingleTest(string dev) {
  int exit_code, mux_index, disk_num, cpld_num;
  string result, sled_name, hdd_name, hdd_str, cpld_hex;
  string mux[] = {"10", "20", "01", "02", "04", "08"};
  string hdd[] = {"01", "02", "04", "08"};
  string cpld[] = {"FE", "FD", "FB", "F7", "EF", "DF", "BF", "7F"};

  OutputMsg("Disable Sensor Polling", true);
  ExecCmd("ipmitool raw 0x30 0xd0 0x50 0x00", &result, &exit_code, true);
  LogProcess("golden", dev);

  for (int loop=1; loop<gsLoopNum+1; loop++) {
    OutputMsg("\n ===== loop " + to_string(loop) + " =====");
    disk_num = 0;
    for (int sled_count=0; sled_count<3; sled_count++) {
      OutputMsg("SLED" + to_string(sled_count), true);
      for (int mux_count=0; mux_count<2; mux_count++) {
        for (int disk_count=0; disk_count<(sizeof(hdd)/sizeof(*hdd)); disk_count++) {
          for (int count=0; count<2; count++) {
            mux_index = sled_count * 2 + mux_count;
            OutputMsg("Switch to mux " + mux[mux_index], true);
            ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe2 0x00 0x00 0x" + mux[mux_index], &result, &exit_code, true);
            if (count == 0){
              OutputMsg("Disable HDD" + to_string(disk_num));
              ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe8 0x00 0x02 0x" + hdd[disk_count], &result, &exit_code, true);
              cpld_hex = cpld[mux_count * 4 + disk_count];
            } else {
              OutputMsg("Enable disk" + to_string(disk_num));
              ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe8 0x00 0x02 0x00", &result, &exit_code, true);
              cpld_hex = "FF";
            }
            sleep(20);
            OutputMsg("Switch to SLED " + to_string(sled_count) + " for CPLD");
            ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe2 0x00 0x00 0x" + mux[sled_count * 2 + 1], &result, &exit_code, true);
            ExecCmd("ipmitool raw 0x06 0x52 0x01 0x22 0x00 0x09 0x" + cpld_hex, &result, &exit_code, true);
          }
          LogProcess("loop" + to_string(loop) + "_SLED" + to_string(sled_count) + "_HDD" + to_string(disk_num), dev);
          disk_num ++;
        }
      }
    }
  }

  OutputMsg("Enable Sensor Polling", true);
  ExecCmd("ipmitool raw 0x30 0xd0 0x50 0x01", &result, &exit_code, true);
}

void AllTest(string dev) {
  int exit_code, mux_index;
  string result, dir_name, hdd_str;
  string mux[] = {"10", "20", "01", "02", "04", "08"};

  OutputMsg("Disable Sensor Polling", true);
  ExecCmd("ipmitool raw 0x30 0xd0 0x50 0x00", &result, &exit_code, true);

  for (int loop=0; loop<gsLoopNum+1; loop++) {
    OutputMsg("\n ===== loop " + to_string(loop) + " =====");
    for (int count=0; count<2; count++) {
      if (loop == 0)
        dir_name = "golden";
      else
        dir_name = "loop" + to_string(loop);
      for (int sled=0; sled<3; sled++) {
        OutputMsg("SLED" + to_string(sled), true);
        for (int mux_count=0; mux_count<2; mux_count++) {
          mux_index = sled * 2 + mux_count;
          OutputMsg("Switch to mux " + mux[mux_index]);
          ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe2 0x00 0x00 0x" + mux[mux_index], &result, &exit_code, true);
          if (count == 0) {
            OutputMsg("Disable HDD");
            ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe8 0x00 0x02 0x0f", &result, &exit_code, true);
          } else {
            OutputMsg("Enable HDD");
            ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe8 0x00 0x02 0x00", &result, &exit_code, true);
          }
          sleep(2);
        }
      }

      for (int sled=0; sled<3; sled++) {
        mux_index = sled * 2 + 1;
        OutputMsg("Switch to SLED " + to_string(sled) + " for CPLD", true);
        ExecCmd("ipmitool raw 0x06 0x52 0x01 0xe2 0x00 0x00 0x" + mux[mux_index], &result, &exit_code, true);
        OutputMsg("Turn back to be Blue all HDD LED");
        ExecCmd("ipmitool raw 0x06 0x52 0x01 0x22 0x00 0x09 0xFF", &result, &exit_code, true);
      }

      if (count == 0)
        dir_name += "_off";
      else
        dir_name += "_on";
      sleep(35);
      LogProcess(dir_name, dev);
    }
  }

  OutputMsg("Enable Sensor Polling", true);
  ExecCmd("ipmitool raw 0x30 0xd0 0x50 0x01", &result, &exit_code, true);
}

void ShowHelp(char* file_name) {
  cout <<
    "Usage:\n"
    << file_name << " -l [loop number] -t [run type]\n"
    "  -l  Total loop number\n"
    "  -t  Run test with single disk or all disks\n"
    "      0: single disk\n"
    "      1: all disks\n"
    "[Options]\n"
    "  -f  Fail stop\n"
    "  -v  Show version\n"
    "  -h  Show this\n" << endl;
  exit(1);
}

void ProcessArgs(int argc, char** argv) {
  const char* const short_opts = "l:t:vfh";
  const option long_opts[] = {
    {"loop-num", required_argument, nullptr, 'l'},
    {"run-type", required_argument, nullptr, 't'},
    {"version", no_argument, nullptr, 'v'},
    {"fail-stop", no_argument, nullptr, 'f'},
    {"help", no_argument, nullptr, 'h'}
  };

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt) break;

    switch (opt) {
      case 'l':
        gsLoopNum = stoi(optarg);
        break;
      case 't':
        gsRunType = stoi(optarg);
        break;
      case 'f':
        gsFailStop = true;
        break;
      case 'v':
        printf("%c %c", &VERSION, &BUILD);
        exit(0);
      case 'h':
      case '?':
      default:
        ShowHelp(argv[0]);
        break;
    }
  }

  if (gsLoopNum == -1 || (gsRunType != 0 && gsRunType != 1))
    ShowHelp(argv[0]);

  struct stat buf;
  if (stat(&RUNNING_LOG[0], &buf) == 0) remove(&RUNNING_LOG[0]);

  string msg;
  OutputMsg("loop number: " + to_string(gsLoopNum));
  msg = "Run type: ";
  (gsRunType == 0) ? msg += "Single disk" : msg += "All disks";
  OutputMsg(msg);
  msg = "Fail stop: ";
  gsFailStop ? msg += "True" : msg += "False";
  OutputMsg(msg);
}

int main(int argc, char *argv[]) {
  int exit_code;
  string dev;

  cout << VERSION << "." << BUILD << endl;
  ProcessArgs(argc, argv);

  ExecCmd("ls /dev/ | grep 'sd[a-z]*$'", &dev, &exit_code);
  if (dev.empty()) {
    cout << "Can't find any disk in this SUT !!" << endl;
    exit(1);
  }

  CreateDir(LOG_DIR, true);
  CreateDir(SMART_LOG_DIR, true);

  if (gsRunType == 0)
    SingleTest(dev);
  else
    AllTest(dev);

  return 0;
}
